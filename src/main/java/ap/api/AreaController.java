package ap.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ap.model.AreaQuiz;
import ap.service.AreaServiceImp;

@Controller
@RequestMapping("/area")
public class AreaController {
    @Autowired
    private AreaServiceImp areaService;

    

    @RequestMapping(value = "/getarea", method = RequestMethod.POST,produces={"application/json"},consumes={"application/json"})
    public @ResponseBody AreaQuiz getArea(@RequestBody AreaQuiz areaQuiz) {
     
    	System.out.println("Came here");
    	
    	return areaService.getArea(areaQuiz);
    }

}