package ap.service;

import org.springframework.stereotype.Service;

import ap.model.AreaQuiz;

@Service
public class AreaServiceImp implements AreaService {
	
	
	@Override
	public AreaQuiz getArea(AreaQuiz areaQuiz){
		
	if(areaQuiz.getUserId()==1){
		
	areaQuiz.setArea((float) Math.pow(areaQuiz.getLength(), 2));	
		
	}else{
		double pie=3.142;
		areaQuiz.setArea((float)( Math.pow(areaQuiz.getLength(), 2)*pie));
		
	}
		
		
		
	return areaQuiz;	
	}
	
	

}
