package ap.service;

import ap.model.AreaQuiz;

public interface AreaService {
	
	public AreaQuiz getArea(AreaQuiz areaQuiz);

}
